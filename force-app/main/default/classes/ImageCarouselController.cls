public without sharing class ImageCarouselController {
    /**
     * Endpoint that retrieves pictures associated with a record
     * @param recordId Record Id
     * @return List of ContentVersion holding the pictures
     */
    @AuraEnabled(cacheable=true)
    public static List<ContentVersion> getImages(Id recordId) {

        List<ContentDocumentLink> links = [
            SELECT Id, LinkedEntityId, ContentDocumentId
            FROM ContentDocumentLink
            WHERE
                LinkedEntityId = :recordId
                AND ContentDocument.FileType IN ('PNG', 'JPG', 'JPEG', 'GIF')
        ];


        if (links.isEmpty()) {
            return null;
        }

        Set<Id> contentIds = new Set<Id>();

        for (ContentDocumentLink link : links) {
            contentIds.add(link.ContentDocumentId);
        }

        return [
            SELECT Id, Title
            FROM ContentVersion
            WHERE ContentDocumentId IN :contentIds AND IsLatest = true
            ORDER BY CreatedDate
        ];
    }
}