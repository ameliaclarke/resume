import { LightningElement, wire, api } from 'lwc';
import getImages from '@salesforce/apex/ImageCarouselController.getImages';


export default class ImageCarousel extends LightningElement {
    @api recordId;
    urls;
    images;

    @wire(getImages, { recordId: '$recordId' })
    wiredPictures(images) {
        this.images = images;
        if (images.data) {
            const files = images.data;
            if (Array.isArray(files) && files.length) {
                this.urls = files.map(
                    (file) =>
                        '/sfc/servlet.shepherd/version/download/' + file.Id
                );
            } else {
                this.urls = null;
            }
        }

        var divblock = this.template.querySelector('.slds-carousel__content');
        console.log('divblock', divblock);
        if (divblock) {
            this.template.querySelector('.slds-carousel__content').className = 'slds-hide';
        }
    }

}