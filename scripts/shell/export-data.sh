#!/bin/bash
#
# Exports resume data
# Example: scripts/shell/export-data.sh


QUERY=" \
SELECT AccountId, Email, FirstName, Languages__c, LastName, Level__c, LinkedIn__c, MailingAddress, MailingCity, MailingCountry, MailingGeocodeAccuracy, MailingLatitude, MailingLongitude, MailingPostalCode, MailingState, MailingStreet, MobilePhone, Name, Personal_Summary__c, Phone, Resume__c, Salutation, Title, Trailhead__c, \
( \
    SELECT Date__c, Duration__c, End_Date__c, Id, Location__c, Name, Person__c, Responsibility__c, Role__c, Start_Date__c \
    FROM Experience__r \
), ( \
    SELECT Client__c, Description__c, Experience__c, Id, Name, Person__c \
    FROM Projects__r \
), ( \
    SELECT Date__c, Details__c, Educational_Institution__c, End_Date__c, Id, Location__c, Name, Person__c, Start_Date__c \
    FROM Education__r \
), ( \
    SELECT Date__c, Id, Issue_Date__c, Issuing_Organisation__c, Name, Person__c, Type__c \
    FROM Certifications__r \
), ( \
    SELECT Date__c, End_Date__c, Id, Location__c, Name, Person__c, Responsibility__c, Role__c, Start_Date__c  \
    FROM Community__r \
), ( \
    SELECT Id, Name, Person__c, Type__c \
    FROM Key_Skills__r \
) \
FROM Contact \
WHERE Name = 'Amelia Clarke' \
"

sfdx force:data:tree:export -q "${QUERY}" -d data



